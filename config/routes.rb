Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  root 'products#index', as: 'home'

  get 'about' => 'pages#about'
  
  resources :products do
  	resources :reviews
  end
end
