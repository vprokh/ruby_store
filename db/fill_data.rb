require 'faker'

image_dir = 'app/assets/sampleImages/'
products = []
reviews = []

product_count = 30
review_count = 10

(1..product_count).each do
  product = {
    title: Faker::Commerce.product_name,
    description: Faker::Lorem.sentence(word_count: 5),
    price: Faker::Commerce.price
  }
  products.push(product)
end

products = Product.create(products)


Product.all.each do |product|
  image_path = Dir[image_dir + '*.jpg'].sample
  image_name = image_path.split('/').last

  puts image_name

  product.image.attach(io: File.open(image_path), filename: image_name);
  product.save! 
end


Product.all.each do |product|

  (1..review_count).each do

    puts 'Review added'

    review = {
        username: Faker::Internet.user_name,
        review_text: Faker::Lorem.sentence(word_count: 2),
        product: product
    }
    reviews.push(review)
  end
end

reviews = Review.create(reviews)